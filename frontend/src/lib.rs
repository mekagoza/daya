extern crate combine;
#[cfg(test)]
extern crate proptest;

pub mod ast;
pub mod lex;
pub mod token;

pub use self::lex::scan;
pub use self::token::*;

#[cfg(test)]
pub mod strat {
    pub use crate::token::strat::*;
}
