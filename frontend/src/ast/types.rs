use crate::ast::{Expr, Value, Var};

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Kind {
    Unit,
    FKind(FKind),
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Type {
    Unit,
    Def(DefTy),
    PureFun(PureFun),
    ImpureFun(ImpureFun),
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Sig {
    Unit,
    Def(Def),
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Def {
    pub label: String,
    pub kind: Kind,
    pub constructors: Vec<(Box<Expr>, Box<Type>)>,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct FKind {
    pub arg: Var,
    pub arg_ty: Box<Type>,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct DefTy {
    pub label: String,
    pub index: Box<Expr>,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct PureFun {
    pub arg: Var,
    pub arg_ty: Box<Type>,
    pub out_ty: Box<Type>,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ImpureFun {
    pub arg: Var,
    pub arg_ty: Box<Type>,
    pub alpha: Box<Type>,
    pub out_ty: Box<Type>,
    pub beta: Box<Type>,
}
