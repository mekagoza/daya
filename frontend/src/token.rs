use std::borrow::Cow;

pub const KEYWORDS: &[&str] = &[
    "let", "in", "match", "with", "fun", "return", "shift", "reset",
];

pub const OPS: &[&str] = &["+", "-", "*", "/", "<=>", "==", "!=", "<", ">", "<=", ">="];

pub const PUNCTS: &[&str] = &["(", ")", "{", "}", "[", "]", ":", ";", "->"];

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Token<'a> {
    Punct(Punct),
    Keyword(Keyword),
    Op(Op),
    Identifier(Cow<'a, str>),
    Int(i64),
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Keyword {
    Let,
    In,
    Match,
    With,
    Fun,
    Return,
    Shift,
    Reset,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Punct {
    LParen,
    RParen,
    LBrace,
    RBrace,
    LBracket,
    RBracket,
    Colon,
    Semicolon,
    Arrow,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Op {
    Plus,
    Minus,
    Mul,
    Div,
    Cmp,
    Eq,
    Neq,
    Lt,
    Gt,
    Leq,
    Geq,
}

#[cfg(test)]
pub mod strat {
    use super::*;
    use proptest::prelude::*;
    use proptest::sample::select;
    use std::borrow::Cow;

    pub fn valid_id() -> impl Strategy<Value = Cow<'static, str>> {
        "[a-zA-Z][a-zA-Z0-9_\\-\\?]*".prop_map(From::from)
    }

    pub fn valid_op() -> impl Strategy<Value = &'static str> {
        select(OPS)
    }

    pub fn valid_keyword() -> impl Strategy<Value = &'static str> {
        select(KEYWORDS)
    }

    pub fn valid_punct() -> impl Strategy<Value = &'static str> {
        select(PUNCTS)
    }

    pub fn keyword() -> impl Strategy<Value = Keyword> {
        use Keyword::*;

        select(vec![Let, In, Match, Fun, Return, Shift, Reset])
    }

    pub fn op() -> impl Strategy<Value = Op> {
        use Op::*;

        select(vec![Plus, Minus, Mul, Div, Cmp, Eq, Neq, Lt, Gt, Leq, Geq])
    }

    pub fn punct() -> impl Strategy<Value = Punct> {
        use Punct::*;

        select(vec![
            LParen, RParen, LBrace, RBrace, LBracket, RBrace, Colon, Semicolon, Arrow,
        ])
    }

    pub fn token() -> impl Strategy<Value = Token<'static>> {
        prop_oneof![
            keyword().prop_map(Token::Keyword),
            op().prop_map(Token::Op),
            punct().prop_map(Token::Punct),
            any::<i64>().prop_map(Token::Int),
            valid_id().prop_map(Token::Identifier),
        ]
    }
}
