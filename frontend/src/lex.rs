use combine::char::{alpha_num, char, digit, letter, space, string};
use combine::error::ParseError;
use combine::parser::choice::choice;
use combine::parser::combinator::{attempt, recognize};
use combine::parser::repeat::many;
use combine::stream::easy::Errors;
use combine::stream::state::{SourcePosition, State};
use combine::stream::Stream;
use combine::{many1, sep_by, skip_many, value, Parser};

use crate::{Keyword, Op, Punct, Token};

use std::borrow::Cow;

pub fn scan(
    input: &str,
) -> Result<(Vec<Token>, State<&str, SourcePosition>), Errors<char, &str, SourcePosition>> {
    let mut parser = sep_by(token(), skip_many(space()));
    parser.easy_parse(State::new(input))
}

fn token<I>() -> impl Parser<Input = I, Output = Token<'static>>
where
    I: Stream<Item = char>,
    I::Error: PError<I>,
{
    choice((
        attempt(keyword().map(Token::Keyword)),
        attempt(punct().map(Token::Punct)),
        attempt(op().map(Token::Op)),
        attempt(identifier()),
        attempt(int()),
    ))
}

fn op<I>() -> impl Parser<Input = I, Output = Op>
where
    I: Stream<Item = char>,
    // Necessary due to rust-lang/rust#24159
    I::Error: PError<I>,
{
    use Op::*;

    choice((
        attempt(string("+").map(|_| Plus)),
        attempt(string("-").map(|_| Minus)),
        attempt(string("*").map(|_| Mul)),
        attempt(string("/").map(|_| Div)),
        attempt(string("==").map(|_| Eq)),
        attempt(string("!=").map(|_| Neq)),
        attempt(string("<=>").map(|_| Cmp)),
        attempt(string("<=").map(|_| Leq)),
        attempt(string(">=").map(|_| Geq)),
        attempt(string("<").map(|_| Lt)),
        attempt(string(">").map(|_| Gt)),
    ))
}

fn punct<I>() -> impl Parser<Input = I, Output = Punct>
where
    I: Stream<Item = char>,
    I::Error: PError<I>,
{
    use Punct::*;

    choice((
        char('(').map(|_| LParen),
        char(')').map(|_| RParen),
        char('{').map(|_| LBrace),
        char('}').map(|_| RBrace),
        char('[').map(|_| LBracket),
        char(']').map(|_| RBracket),
        char(':').map(|_| Colon),
        char(';').map(|_| Semicolon),
        string("->").map(|_| Arrow),
    ))
}

fn keyword<I>() -> impl Parser<Input = I, Output = Keyword>
where
    I: Stream<Item = char>,
    I::Error: PError<I>,
{
    use Keyword::*;

    choice((
        string("in").map(|_| In),
        string("let").map(|_| Let),
        string("fun").map(|_| Fun),
        string("with").map(|_| With),
        string("match").map(|_| Match),
        string("shift").map(|_| Shift),
        attempt(string("reset").map(|_| Reset)),
        string("return").map(|_| Return),
    ))
}

fn identifier<I>() -> impl Parser<Input = I, Output = Token<'static>>
where
    I: Stream<Item = char>,
    I::Error: PError<I>,
{
    let valid_char = choice((alpha_num(), char('-'), char('_'), char('?')));

    let parser = recognize((letter(), many::<String, _>(valid_char)))
        .map(|s: String| Token::Identifier(Cow::from(s)))
        .message("Invalid identifier");

    parser
}

fn int<I>() -> impl Parser<Input = I, Output = Token<'static>>
where
    I: Stream<Item = char>,
    I::Error: PError<I>,
{
    let sign = choice((char('+').map(|_| 1), char('-').map(|_| -1))).or(value(1));
    let int = recognize((sign, many1::<String, _>(digit())));
    let parser = int
        .map(|s: String| {
            let i = i64::from_str_radix(&s, 10).unwrap();
            Token::Int(i)
        })
        .message("Invalid integer");

    parser
}

/// trait alias because i'm lazy
trait PError<I>: ParseError<I::Item, I::Range, I::Position>
where
    I: Stream,
{
}

impl<T, I> PError<I> for T
where
    I: Stream,
    T: ParseError<I::Item, I::Range, I::Position>,
{
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::strat::{valid_id, valid_keyword, valid_op, valid_punct};
    use crate::{Keyword, Punct};
    use proptest::prelude::*;

    proptest! {
        #[test]
        fn test_identifier(s in valid_id()) {
            identifier().easy_parse(State::new(&s[..])).unwrap();
        }

        #[test]
        fn test_int(i in any::<i64>()) {
            let s = i.to_string();
            int().easy_parse(State::new(&s[..])).unwrap();
        }

        #[test]
        fn test_op(s in valid_op()) {
            op().easy_parse(State::new(&s[..])).unwrap();
        }

        #[test]
        fn test_punct(s in valid_punct()) {
            punct().easy_parse(State::new(&s[..])).unwrap();
        }

        #[test]
        fn test_keyword(s in valid_keyword()) {
            keyword().easy_parse(State::new(&s[..])).unwrap();
        }

        #[test]
        fn test_expr(x in valid_id(), f in valid_id(), op_str in valid_op()) {

            let input = format!("(let [{} 5] in ({} ({} {} 8)))", x, f, op_str, x);

            let (op_t, _) = op().easy_parse(State::new(&op_str[..])).unwrap();

            let (tokens, _) = scan(&input).unwrap();

            let expected = vec![
                Token::Punct(Punct::LParen),
                Token::Keyword(Keyword::Let),
                Token::Punct(Punct::LBracket),
                Token::Identifier(x.clone()),
                Token::Int(5),
                Token::Punct(Punct::RBracket),
                Token::Keyword(Keyword::In),
                Token::Punct(Punct::LParen),
                Token::Identifier(f),
                Token::Punct(Punct::LParen),
                Token::Op(op_t),
                Token::Identifier(x),
                Token::Int(8),
                Token::Punct(Punct::RParen),
                Token::Punct(Punct::RParen),
                Token::Punct(Punct::RParen)
            ];

            assert_eq!(tokens, expected);
        }
    }
}
