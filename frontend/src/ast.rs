pub mod types;

pub use self::expr::*;
pub use self::value::*;

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Kind {}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Type {}

pub mod value {
    use crate::ast::{Expr, Type};

    #[derive(Debug, PartialEq, Eq, Clone)]
    pub enum Value {
        Unit,
        Var(Var),
        Fun(Fun),
        RecFun(RecFun),
        Const(VConst),
    }

    #[derive(Debug, PartialEq, Eq, Clone)]
    pub struct Var(pub String);

    #[derive(Debug, PartialEq, Eq, Clone)]
    pub struct Fun {
        pub arg: Var,
        pub arg_ty: Type,
        pub body: Box<Expr>,
        pub body_ty: Type,
    }

    #[derive(Debug, PartialEq, Eq, Clone)]
    pub struct RecFun {
        pub arg: Var,
        pub arg_ty: Type,
        pub body: Box<Expr>,
        pub body_ty: Type,
    }

    #[derive(Debug, PartialEq, Eq, Clone)]
    pub struct VConst {
        pub index: u32,
        pub body: Box<Value>,
    }
}

pub mod expr {
    use crate::ast::{Type, Value, Var};

    #[derive(Debug, PartialEq, Eq, Clone)]
    pub enum Expr {
        Value(Value),
        App(App),
        Let(LetIn),
        Const(Const),
        Match(Match),
        Shift(Shift),
        Reset(Reset),
    }

    #[derive(Debug, PartialEq, Eq, Clone)]
    pub struct App {
        pub func: Box<Expr>,
        pub func_ty: Type,
        pub arg: Box<Expr>,
        pub arg_ty: Type,
    }

    #[derive(Debug, PartialEq, Eq, Clone)]
    pub struct LetIn {
        pub binding: Var,
        pub bind_expr: Box<Expr>,
        pub bind_ty: Type,
        pub body: Box<Expr>,
    }

    #[derive(Debug, PartialEq, Eq, Clone)]
    pub struct Const {
        pub index: u32,
        pub body: Box<Expr>,
    }

    #[derive(Debug, PartialEq, Eq, Clone)]
    pub struct Match {
        pub expr: Box<Expr>,
        pub exp_bind: Var,
        pub index_ty: Type,
        pub index: u32,
        pub ret: Type,
        pub arms: Vec<(Const, Box<Expr>)>,
    }

    #[derive(Debug, PartialEq, Eq, Clone)]
    pub struct Shift {
        pub cont: Var,
        pub cont_ty: Type,
        pub body: Box<Expr>,
    }

    #[derive(Debug, PartialEq, Eq, Clone)]
    pub struct Reset {
        pub body: Box<Expr>,
    }
}
